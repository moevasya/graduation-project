import {React , useState, useEffect} from 'react';
import './App.css';
import HomePage from './views/HomePage.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import News from './views/News';
import Weather from './views/Weather.js'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import axios from 'axios';
import Tour from './views/Tour.js';

  
function App() {
  const url = `http://newsapi.org/v2/everything?q=apple&from=2021-01-09&to=2021-01-09&sortBy=popularity&apiKey=d3207a7c9ea04f0aa742b10d138f1a7e`;
  const [newsApi, getNews] = useState('');

  useEffect(() => {
    axios.get(url).then((response) => {
      getNews(response.data);
    })
    .catch(error => console.error(`Error: ${error}`))
  }, [url]);
  
  return (
    <>
    <Router>
    <Switch>
      <Route exact path="/">
    <HomePage API={newsApi}/>
    </Route>
    <Route path="/News:id">
      <News API={newsApi}/>
    </Route>
    <Route path="/Weather:id">
      <Weather />
    </Route>
    <Route path="/Tour">
      <Tour />
    </Route>
    </Switch>

    </Router>
    </>
  );
}

export default App;
