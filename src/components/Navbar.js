import React from "react";
import styled from "styled-components";
import {Link} from 'react-router-dom'
export default function Navbar() {
  return (
    <NavbarDiv>
      <Title>Jordan Now</Title>
      <Link to="/">
      <Home>Home</Home>
      </Link>
      <Link to="News0">
      <News>News</News>
      </Link>
      <Link to="WeatherAmman">
      <Weather>Weather</Weather>
      </Link>
      <Link to="Tour">
      <Tour>Tour</Tour>
      </Link>
      <Link to="About Us">
      <AboutUs>About Us</AboutUs>
      </Link>
    </NavbarDiv>
  );
}

const NavbarDiv = styled.div`
  width: 100%;
  height: 50px;
  background-color: #ffffff;
  position: absolute;
  left: 0;
  top: 0;
  display: flex;
  flex-direction: row;
`;

const Title = styled.h2`
  position: absolute;
  left: 1.88%;
  right: 85.23%;

  font-family: Roboto;
  font-style: normal;
  font-weight: Bolder;
  font-size: 30px;
  line-height: 41px;
  display: flex;
  align-items: center;

  /* color 1 */

  color: #31255a;
`;
const Home = styled.h2`
  position: absolute;
  left: 56%;
  right: 34%;

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  line-height: 41px;
  display: flex;
  align-items: center;
  text-align: center;

  /* color 1 */

  color: #31255a;
`;
const News = styled.h2`
  position: absolute;
  left: 65%;
  right: 25%;

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  line-height: 41px;
  display: flex;
  align-items: center;
  text-align: center;

  /* color 1 */

  color: #31255a;
`;

const Weather = styled.h2`
  position: absolute;
  left: 73%;
  right: 17%;

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  line-height: 41px;
  display: flex;
  align-items: center;
  text-align: center;

  /* color 1 */

  color: #31255a;
`;
const Tour = styled.h2`
  position: absolute;
  left: 83%;
  right: 7.0%;

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  line-height: 41px;
  display: flex;
  align-items: center;
  text-align: center;

  /* color 1 */

  color: #31255a;
`;

const AboutUs = styled.h2`
  position: absolute;
  left: 90%;
  right: 0;

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  line-height: 41px;
  display: flex;
  align-items: center;
  text-align: center;

  /* color 1 */

  color: #31255a;
`;
