import { React, useState, useEffect } from "react";
import styled from "styled-components";
import Navbar from "../components/Navbar.js";
import Footer from "../components/Footer.js";
import axios from "axios";
import Sky from "../images/sky.jpg";
import { Link, useParams } from "react-router-dom";
export default function Weather() {
  const { id } = useParams() ;

  const apiKey = "ca98617be22b7c8ea4a8d444241a7661";
  const urlWeather = `http://api.openweathermap.org/data/2.5/forecast?q=${id}&appid=${apiKey}`;
  const [weatherAPI, getWeather] = useState("");

  useEffect(() => {
    axios
      .get(urlWeather)
      .then((response) => {
        getWeather(response.data);
      })
      .catch((error) => console.error(`Error: ${error}`));
  }, [urlWeather]);
  return weatherAPI ? (
    <>
      <Navbar />
      <MainCont>
        <FirstTitleContainer>
          <FirstTitle>
            {" "}
            Current Weather Condition in {weatherAPI.city["name"]}
          </FirstTitle>
          <FirstDivider></FirstDivider>
          <FirstContainer>
            <FirstImage src={Sky}></FirstImage>
            <FirstGlassEffect></FirstGlassEffect>
            <FirstSubCont>
              <table class="Table">
                <tbody>
                  <tr>
                    <th className="TableElements">Temp</th>
                    <th className="TableElements">
                      {Math.trunc(weatherAPI.list["0"].main["temp"] - 270)}
                      &#176;
                    </th>
                    <th className="TableElements">Real Feel</th>
                    <th className="TableElements">
                      {Math.trunc(
                        weatherAPI.list["0"].main["feels_like"] - 270
                      )}
                      &#176;
                    </th>
                  </tr>

                  <tr>
                    <th className="TableElements">Ground Level</th>
                    <th className="TableElements">
                      {weatherAPI.list["0"].main["grnd_level"]}
                    </th>
                    <th className="TableElements">Pressure</th>
                    <th className="TableElements">
                      {weatherAPI.list["0"].main["pressure"]}
                    </th>
                  </tr>
                  <tr>
                    <th className="TableElements">Max Tempreture</th>
                    <th className="TableElements">
                      {Math.trunc(weatherAPI.list["0"].main["temp_max"] - 270)}
                      &#176;
                    </th>
                    <th className="TableElements">Min Tempreture</th>
                    <th className="TableElements">
                      {Math.trunc(weatherAPI.list["0"].main["temp_min"] - 270)}
                      &#176;
                    </th>
                  </tr>
                  <tr>
                    <th className="TableElements">Humidity</th>
                    <th className="TableElements">
                      {weatherAPI.list["0"].main["humidity"]}%
                    </th>
                    <th className="TableElements">Description</th>
                    <th className="TableElements">
                      {weatherAPI.list["0"].weather["0"].description}
                    </th>
                  </tr>
                </tbody>
              </table>
            </FirstSubCont>
            <CityName>
              {weatherAPI.city["name"]}, {weatherAPI.city.country}
            </CityName>
            <Temp>
              {" "}
              {Math.trunc(weatherAPI.list["0"].main["temp"] - 270)}&#176;
            </Temp>
            <Icon>
              <img
                className=""
                src={`https://openweathermap.org/img/wn/${weatherAPI.list["0"].weather["0"].icon}.png`}
              ></img>
              <h5>{weatherAPI.list["0"].weather["0"].main}</h5>
            </Icon>
          </FirstContainer>
        </FirstTitleContainer>
        <SecondTitle>
          Extended Forecast in {weatherAPI.city["name"]}
        </SecondTitle>
        <SecondDivider></SecondDivider>
        <SecondContainer>
          <SecondGlassEffect>
            <SecondSubCont>
              <IconContainer>
                {(weatherAPI.list["9"].dt_txt + "").substring(0, 11)}
                <ExtendIcon
                  src={`https://openweathermap.org/img/wn/${weatherAPI.list["8"].weather["0"].icon}.png`}
                ></ExtendIcon>
                {weatherAPI.list["9"].weather["0"].description}
                <br></br>
                {Math.trunc(weatherAPI.list["9"].main["temp"] - 270)}&#176;
              </IconContainer>
              <IconContainer>
                {(weatherAPI.list["17"].dt_txt + "").substring(0, 11)}
                <ExtendIcon
                  src={`https://openweathermap.org/img/wn/${weatherAPI.list["16"].weather["0"].icon}.png`}
                ></ExtendIcon>
                {weatherAPI.list["17"].weather["0"].description}
                <br></br>
                {Math.trunc(weatherAPI.list["17"].main["temp"] - 270)}&#176;
              </IconContainer>
              <IconContainer>
                {(weatherAPI.list["25"].dt_txt + "").substring(0, 11)}
                <ExtendIcon
                  src={`https://openweathermap.org/img/wn/${weatherAPI.list["24"].weather["0"].icon}.png`}
                ></ExtendIcon>
                {weatherAPI.list["25"].weather["0"].description}
                <br></br>
                {Math.trunc(weatherAPI.list["25"].main["temp"] - 270)}&#176;
              </IconContainer>
            </SecondSubCont>
          </SecondGlassEffect>
        </SecondContainer>
        <ThirdTitle>City List</ThirdTitle>
        <ThirdDivider></ThirdDivider>
        <ThirdContainer>
          <table class="Table">
          <tbody>
          <br></br>

              <tr>
                <Link className="TableElements Align" to={"/WeatherAmman"}>
                <td className="TextAlign">Amman</td>
                </Link>
                <Link className="TableElements Align" to={"/WeatherIrbid"}>
                <td className="TextAlign" >Irbid</td>
                </Link>
                <Link className="TableElements Align" to={"/WeatherWadi Musa"}>
                <td className="TextAlign" >Maan </td>
                </Link>
                <Link className="TableElements Align" to={"/WeatherSalt"}>
                <td className="TextAlign" >Salt</td>
                </Link>
              </tr>
            
              <br></br>

              <tr>
                <Link className="TableElements Align" to={"/WeatherAqaba"}>
                <td className="TextAlign" >Aqaba</td>
                </Link>
                <Link className="TableElements Align" to={"/WeatherJerash"}>
                <td className="TextAlign" >Jerash</td>
                </Link>
                <Link className="TableElements Align" to={"/WeatherZarqa"}>
                <td className="TextAlign" >Zarqa</td>
                </Link>
                <Link className="TableElements Align" to={"/WeatherMafraq"}>
                <td className="TextAlign" >Mafraq</td>
                </Link>
              </tr>
              <br></br>

              <tr>
                
                <Link className="TableElements Align" to={"/WeatherAdjlun"}>
                <td className="TextAlign" >Ajlun</td>
                </Link>
                <Link className="TableElements Align" to={"/WeatherKarak"}>
                <td className="TextAlign" >Karak</td>
                </Link>
                <Link className="TableElements Align" to={"/WeatherAl Tafile"}>
                <td className="TextAlign" >Al Tafile</td>
                </Link>
                <Link className="TableElements Align" to={"/WeatherMadaba"}>
                <td className="TextAlign" >Madaba</td>
                </Link>
              </tr>
            </tbody>
          </table>
        </ThirdContainer>
      </MainCont>
      <Footer page={"Weather"} />
    </>
  ) : null;
}

const MainCont = styled.div`
display:flex;
flex-direction:column;
1
`;
const FirstTitleContainer = styled.div`
  position: absolute;
  width: 939px;
  height: 143px;
  left: 65px;
  top: 50px;
`;
const FirstTitle = styled.h1`
  position: absolute;
  left: 0%;
  right: -30.67%;
  top: 0%;
  bottom: 0%;

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 30px;
  line-height: 94px;
  display: flex;
  align-items: center;

  color: #2b235a;
`;
const FirstDivider = styled.div`
  position: absolute;
  width: 1229.01px;
  height: 0px;
  top: 100px;

  border: 1px solid #2b235a;
`;

const FirstContainer = styled.div`
  position: absolute;
  width: 1348px;
  height: 650px;
  left: -65px;
  top: 150px;

  border-radius: 15px;
  background-color: #dddddd;
`;
const FirstImage = styled.img`
  position: absolute;
  left: 0%;
  right: 0%;
  top: 0%;
  bottom: 0%;

  width: 1348px;
  border-radius: 15px;
`;
const FirstGlassEffect = styled.div`
  position: absolute;
  left: 0%;
  right: 0%;
  top: 0%;
  bottom: 36.77%;
  padding-left:5%;
  background: rgba(255, 255, 255, 0.2);
  backdrop-filter: blur(25px);
  /* Note: backdrop-filter has minimal browser support */

  border-radius: 15px;
`;

const FirstSubCont = styled.div`
  position: absolute;
  left: 0%;

  top: 411px;
  height: 239px;
  width: 1348px;

  background: #ffffff;
  /* color three */

  border: 2px solid #75b4e3;
  box-sizing: border-box;
  border-radius: 15px;
`;
const CityName = styled.div`
  position: absolute;
  left: 10%;
  right: 35.97%;
  top: 5.64%;
  bottom: 74.29%;

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 30px;
  line-height: 94px;
  display: flex;
  align-items: center;
  text-align: center;

  /* white */

  color: #ffffff;
`;

const Temp = styled.div`
  position: absolute;
  left: 75.62%;
  right: 1.53%;
  top: 1.22%;
  bottom: 69.87%;

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 50px;
  line-height: 105px;
  display: flex;
  align-items: center;
  text-align: center;

  /* white */

  color: #ffffff;
`;

const Icon = styled.div`
  position: absolute;
  left: 45%;
  top: 30%;
  bottom: 63.77%;
  display: flex;
  flex-direction: column;
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 40px;
  line-height: 47px;
  display: flex;
  align-items: center;
  text-align: center;

  color: #ffffff;
`;

const SecondTitle = styled.div`
  position: absolute;
  left: 67px;
  top: 950px;
  bottom: 57.14%;

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 30px;
  line-height: 94px;
  display: flex;
  align-items: center;

  color: #2b235a;
`;
const SecondDivider = styled.div`
  position: absolute;
  width: 1229px;
  height: 2px;
  left: 65px;
  top: 990px;

  border: 1px solid #2b235a;
`;
const SecondContainer = styled.div`
  position: absolute;
  width: 1348px;
  height: 700px;
  left: 0px;
  top: 1050px;
  background: url(${Sky});
  border-radius: 15px;
`;
const SecondGlassEffect = styled.div`
  display: flex;
  diplay-direction: column;
  position: absolute;
  left: 0%;
  right: 0%;
  top: 0%;
  bottom: 0%;

  background: rgba(255, 255, 255, 0.2);
  backdrop-filter: blur(25px);
  /* Note: backdrop-filter has minimal browser support */

  border-radius: 15px;
`;

const SecondSubCont = styled.div`
  display: flex;
  display-direction: row;
  width: 100%;
  margin: auto;
  text-align: center;
  padding: 20%;
`;
const ThirdTitle = styled.div`
  position: absolute;
  left: 67px;
  top: 1800px;
  bottom: 57.14%;

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 30px;
  line-height: 94px;
  display: flex;
  align-items: center;

  color: #2b235a;
`;
const ThirdDivider = styled.div`
  position: absolute;
  width: 1229px;
  height: 2px;
  left: 65px;
  top: 1840px;

  border: 1px solid #2b235a;
`;
const ThirdContainer = styled.div`
  position: absolute;
  width: 1348px;
  height: 407px;
  left: 0px;
  top: 1890px;
  background: #75b4e3;
`;
const IconContainer = styled.div`
  width: 400px;
  height: 100%;
  diplay: flex;
  flex-direction: row;
  margin: 0px 5%;
  text-align: center;
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 30px;
  color: #2b235a;
`;

const ExtendIcon = styled.img`
height 150px;
width:150px;


`;
