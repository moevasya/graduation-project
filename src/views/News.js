import React from 'react';
import Navbar from '../components/Navbar.js';
import styled from 'styled-components';
import Carousel from "react-bootstrap/Carousel";
import {useParams} from 'react-router-dom';
import Footer from '../components/Footer.js';
import {Link} from 'react-router-dom';
export default function News(props){
    const News = props.API;
    const { id } = useParams() ;
return News ?(
    <>
<MainCont>
<Navbar />
<PageTitleContainer>
<PageTitle>News</PageTitle>
</PageTitleContainer>
<TopDivider />
<HeadLineDiv>
<HeadLine>{News.articles[id].title}</HeadLine>
</HeadLineDiv>
<ImageContainer>
    <Image src={News.articles[id].urlToImage}></Image>
</ImageContainer>
<DetailsContainer>
    <Details>
    source: {News.articles[id].source.name}<br></br>
<br></br>
        {News.articles[id].description} <b></b>   <a className="Link" target="_blank" href={News.articles[id].url}>Continue Reading</a>
</Details>
</DetailsContainer>
<CaroDiv>
<Carousel className="Carousel" fade={true}>
          <Carousel.Item interval={300000}>
            <img
              className="d-block w-100 h-90 "
              src={News.articles["0"].urlToImage}
              alt="First slide"
            />
            <Mask>
            <Carousel.Caption>
            <Link to="/News0" className="CarouselLinks">
              <h5>{News.articles["0"].title}</h5>
              <p>{News.articles["0"].description}</p>
              </Link>
            </Carousel.Caption></Mask>
          </Carousel.Item>
          
          <Carousel.Item  interval={300000}>
            <img className="d-block w-100 h-90" 
            src={News.articles["1"].urlToImage}
             alt="Third slide" />
            <Mask>
            <Carousel.Caption>
            <Link to="/News1" className="CarouselLinks">
              <h5>{News.articles["1"].title}</h5>
              <p>{News.articles["1"].description}</p></Link>
            </Carousel.Caption></Mask>
          </Carousel.Item>
          <Carousel.Item interval={300000}>
            <img className="d-block w-100 h-90" src={News.articles["2"].urlToImage} alt="Third slide" />
            <Mask>
            <Carousel.Caption>
            <Link to="/News2" className="CarouselLinks">
              <h5>{News.articles["2"].title}</h5>
              <p>
              {News.articles["2"].description}
              </p>
              </Link>
            </Carousel.Caption></Mask>
          </Carousel.Item>
          <Carousel.Item  interval={300000}>
            <img className="d-block w-100 h-90" src={News.articles["3"].urlToImage} alt="Third slide" />
            <Mask>
            <Carousel.Caption>
            <Link to="/News3" className="CarouselLinks">
              <h5>{News.articles["3"].title}</h5>
              <p>
              {News.articles["3"].description}
              </p>
              </Link>
            </Carousel.Caption></Mask>
          </Carousel.Item>
          <Carousel.Item  interval={300000}>
            <img className="d-block w-100 h-90" src={News.articles["4"].urlToImage} alt="Third slide" />
            <Mask>
            <Carousel.Caption>
            <Link to="/News4" className="CarouselLinks">
              <h5>{News.articles["4"].title}</h5>
              <p>
              {News.articles["4"].description}
              </p>
              </Link>

            </Carousel.Caption></Mask>
          </Carousel.Item>
          <Carousel.Item  interval={300000}>
            <img className="d-block w-100 h-90" src={News.articles["5"].urlToImage} alt="Third slide" />
            <Mask>
            <Carousel.Caption>
            <Link to="/News5" className="CarouselLinks">
              <h5>{News.articles["5"].title}</h5>
              <p>
              {News.articles["5"].description}
              </p>
              </Link>

            </Carousel.Caption></Mask>
          </Carousel.Item>
          <Carousel.Item  interval={300000}>
            <img className="d-block w-100 h-90" src={News.articles["6"].urlToImage} alt="Third slide" />
            <Mask>
            <Carousel.Caption>
            <Link to="/News6" className="CarouselLinks">
              <h5>{News.articles["6"].title}</h5>
              <p>
              {News.articles["6"].description}
              </p>
              </Link>

            </Carousel.Caption></Mask>
          </Carousel.Item>
          <Carousel.Item  interval={300000}>
            <img className="d-block w-100 h-90" src={News.articles["7"].urlToImage} alt="Third slide" />
            <Mask>
            <Carousel.Caption>
            <Link to="/News7" className="CarouselLinks">
              <h5>{News.articles["7"].title}</h5>
              <p>
              {News.articles["7"].description}
              </p>
              </Link>

            </Carousel.Caption></Mask>
          </Carousel.Item>
          <Carousel.Item  interval={300000}>
            <img className="d-block w-100 h-90" src={News.articles["8"].urlToImage} alt="Third slide" />
            <Mask>
            <Carousel.Caption>
            <Link to="/News8" className="CarouselLinks">
              <h5>{News.articles["8"].title}</h5>
              <p>
              {News.articles["8"].description}
              </p>
              </Link>

            </Carousel.Caption></Mask>
          </Carousel.Item>
          <Carousel.Item  interval={300000}>
            <img className="d-block w-100 h-90" src={News.articles["9"].urlToImage} alt="Third slide" />
            <Mask>
            <Carousel.Caption>
            <Link to="/News9" className="CarouselLinks">
              <h5>{News.articles["9"].title}</h5>
              <p>
              {News.articles["9"].description}
              </p>
              </Link>

            </Carousel.Caption></Mask>
          </Carousel.Item>
          <Carousel.Item  interval={300000}>
            <img className="d-block w-100 h-90" src={News.articles["10"].urlToImage} alt="Third slide" />
            <Mask>
            <Carousel.Caption>
            <Link to="/News10" className="CarouselLinks">
              <h5>{News.articles["10"].title}</h5>
              <p>
              {News.articles["10"].description}
              </p>
              </Link>

            </Carousel.Caption></Mask>
          </Carousel.Item>
          <Carousel.Item  interval={300000}>
            <img className="d-block w-100 h-90" src={News.articles["11"].urlToImage} alt="Third slide" />
            <Mask>
            <Carousel.Caption>
            <Link to="/News11" className="CarouselLinks">
              <h5>{News.articles["11"].title}</h5>
              <p>
              {News.articles["11"].description}
              </p>
              </Link>

            </Carousel.Caption></Mask>
          </Carousel.Item>
        </Carousel>
        </CaroDiv>
        </MainCont>
<Footer page={"News"} />
</>
) : null;


}
const CaroDiv = styled.div`
position:absolute;
top:300px;
left:0;
width:1360px;
`

const Mask = styled.div`
width: 100%;
height: 18%;
background-color: rgba(0, 0, 0, 0.6);
position: absolute;
top:0;
vertical-align:top;
`;
const MainCont = styled.div`
display: flex;
felx-direction: column;
`
const PageTitleContainer = styled.div`
position: absolute;
width: 201px;
height: 143px;
left: 65px;
top: 117px;
`;

const PageTitle = styled.h1`
position: absolute;
left: 0%;
right: 0%;
top: 0%;
bottom: 0%;

font-family: Roboto;
font-style: normal;
font-weight: normal;
font-size: 40px;
line-height: 94px;
display: flex;
align-items: center;

color: #2B235A
`
const TopDivider = styled.div`
position: absolute;
width: 1229.01px;
height: 0px;
left: 65px;
top: 210px;

border: 2px solid #2B235A;`

const ImageContainer = styled.div`
position: absolute;
width: 1195px;
left: 83px;
top: 330px;
margin : 0px auto;
`
const Image = styled.img`
width:1195px;
height:600px;
`
const HeadLineDiv = styled.div`
position: absolute;
width: 1140px;
height: 115px;
left: 65px;
top: 240px;

background: #FFFFFF;`

const HeadLine = styled.p`
position: absolute;
left: 0%;
right: 0%;
top: 0%;
bottom: 0%;

font-family: Roboto;
font-style: normal;
font-weight: bold;
font-size: 25px;
display: flex;
align-items: center;

/* black */

color: #000000;
`
const DetailsContainer = styled.div`
position: absolute;
width: 1201px;
height: auto;
left: 65px;
top: 1000px;
display:flex;
flex-direction:column;
`
const Details = styled.div`


font-family: Roboto;
font-style: normal;
font-weight: normal;
font-size: 20px;
display: flex;
align-items: center;
display:flex;
flex-direction:column;
color: #000000;`