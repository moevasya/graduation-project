import React from "react";
import styled from "styled-components";
import Navbar from "../components/Navbar.js";
import Petra from "../images/petra.jpg";
import Footer from '../components/Footer.js';

export default function Tour() {
  return (
    <>
      <Navbar />
      <ImageCont>
        <GlassMorph>
          <Name>Laurem</Name>
        </GlassMorph>
      </ImageCont>
      <Description>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
        velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
        occaecat cupidatat non proident, sunt in culpa qui officia deserunt
        mollit anim id est laborum Lorem ipsum dolor sit amet, consectetur
        adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
        magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
        in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
        pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa
        qui officia deserunt mollit anim id est laborum
      </Description>
      <Container>

      </Container>
      <Footer page ={"Weather"}/>
    </>
  );
}

const ImageCont = styled.div`
  position: absolute;
  width: 1360px;
  height: 644px;
  left: 0px;
  top: 89px;
  background-image: url(${Petra});
`;

const GlassMorph = styled.div`
  position: absolute;
  left: 2.72%;
  right: 74.41%;
  top: 4.04%;
  bottom: 78.52%;

  background: rgba(255, 255, 255, 0.2);
  backdrop-filter: blur(25px);
  /* Note: backdrop-filter has minimal browser support */

  border-radius: 15px;
`;

const Name = styled.div`
  position: absolute;
  left: 9.72%;
  right: 74.41%;
  top: 45.07%;
  bottom: 82.66%;

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 75px;
  line-height: 88px;
  display: flex;
  align-items: center;
  text-align: center;

  color: #54416d;
`;

const Description = styled.div`
  position: absolute;
  width: 1275px;
  height: 833px;
  left: 42px;
  top: 757px;
  font-family: Roboto;
font-style: normal;
font-weight: normal;
font-size: 25px;
line-height: 47px;
display: flex;
align-items: center;

color: #000000;
`;
const Container = styled.div`
position: absolute;
width: 1360px;
height: 506px;
left: 0px;
top: 1693px;
background: #75B4E3;
`